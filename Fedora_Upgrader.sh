#!/bin/bash

#####VARIABLES#####
CURRENT_FEDORA=$(rpm -E %fedora)

NEW_FEDORA=$(curl -s https://getfedora.org/en/workstation/download/ | tidy -q 2>/dev/null |grep ".iso" | cut -d "/" -f 8 | head -1)
#NEW_FEDORA=$((CURRENT_FEDORA+1))

####LOG FILE###
LOG_FILE=/var/log/Fedora_upgrader.log
exec &>$LOG_FILE

# Check if the script is run as root only
if [ $EUID -ne 0 ]; then
        echo "This script must be run as root"
        exit 1
fi

####MAIN####

###############################
#      GRAPHICAL VERSION      #
###############################

if [ $CURRENT_FEDORA != $NEW_FEDORA ]; then

	zenity --question --text="You are actually under FEDORA $CURRENT_FEDORA\nA new version of Fedora is available, FEDORA $NEW_FEDORA!\nDo you want to upgrade ?" --width=400 --height=100

	RESPONSE=$?
	echo $RESPONSE

	if [ $RESPONSE -eq 0 ]; then
		echo "yes"
		dnf upgrade -y --refresh
        notify-send -t 3000 -u low "Checking necessary packages..."
        CHECK=rpm -qa | grep "python3-dnf-plugin-system-upgrade" | echo $?
        if [ $CHECK -ne 0 ]; then
	        dnf install -y dnf-plugin-system-upgrade
        fi
		sudo dnf system-upgrade download --refresh --releasever=$NEW_FEDORA
        dnf system-upgrade reboot
	else
		echo "NOPE"
		exit 1
	fi

else
       zenity --info --text="The next version of FEDORA is not available !\nOperation  aborted !\nYou have to wait for it ;)!  " --width=400 --height=100

fi

##################################
#      COMMAND LINE VERSION      #
#################################

#echo "You are actually under FEDORA $CURRENT_FEDORA"

#if [ $CURRENT_FEDORA != $NEW_FEDORA ]; then

	#echo -e "A new version of Fedora is available, FEDORA $NEW_FEDORA!\nDo you want to upgrade ?\n[Yy|nN]"
	#read CHOICE

	#case $CHOICE in
	#	Y|y)
	#		echo "Upgrade started"
	#		dnf upgrade --refresh
#			echo "Checking necessary packages..."
#			CHECK=rpm -qa | grep "python3-dnf-plugin-system-upgrade" || echo $?
#			if [ $CHECK -ne 0 ]; then
#				dnf install -y dnf-plugin-system-upgrade
#			fi
			
#			dnf upgrade-system download --releasesever=$NEW_FEDORA

#			dnf system-upgrade reboot	
#		;;
#		N|n)
#			echo "tu as dis non"
#			echo "Processes halted"
#		;;
#		*)
#			echo "Syntax : $0 [Yy|Nn]"
#	esac

#else
#	echo "Upgrade avorted ! You have to wait for it ;)!  "
#fi
